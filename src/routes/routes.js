const express = require('express')
const app = express()
const router = express.Router()

const TestController = require("../controllers/TestController")

router.get("/", TestController.SayHi)
router.post("/post", TestController.SayTestName)


module.exports = router;