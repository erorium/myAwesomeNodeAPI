class TestController {
  SayHi(req, res) {
    res.send("Hello world!");
  }
  SayTestName(req, res) {
    const { username } = req.body;
    if (username) {
      res.send(`username: ${username}`);
    } else res.send("Not found username!");
  }
}

module.exports = new TestController();
